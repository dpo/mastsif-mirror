***************************
* SET UP THE INITIAL DATA *
***************************

NAME          TRIGON2NE

*   Problem :
*   *********

*   SCIPY global optimization benchmark example Trigonometric02

*   Fit: y = (0,sqrt(8)sin(7(x_i-0.9)^2),sqrt(6)sin(14(x_i-0.9)^2),x_i) + e

*   Source:  Problem from the SCIPY benchmark set
*     https://github.com/scipy/scipy/tree/master/benchmarks/ ...
*             benchmarks/go_benchmark_functions

*   Nonlinear-equation formulation of TRIGON2.SIF

*   SIF input: Nick Gould, Jan 2020

*   classification NOR2-MN-V-V

*   Number of variables

 IE N                   10             $-PARAMETER
*IE N                   100            $-PARAMETER
*IE N                   1000           $-PARAMETER

*   Number of data values (3n+1)

 IM ME        N         3
 IA M         ME        1

*   Useful parameters

 IE 1                   1
 IE 2                   2
 RI RN        N        
 RF ROOT6     SQRT      6.0
 RF ROOT8     SQRT      8.0

VARIABLES

 DO I         1                        N
 X  X(I)
 ND

GROUPS

 XE FA
 DO I         1                        N
 XE FB(I)
 XE FC(I)
 XE FD(I)     X(I)      1.0
 ND

CONSTANTS

    TRIGON2   FA        1.0
 DO I         1                        N
 X  TRIGON2   FD(I)     0.9
 ND

BOUNDS

 FR BOUNDS    'DEFAULT'

START POINT

 DO I         1                        N
 RI RI        I
 R/ START     RI                       RN
 Z  START     X(I)                     START
 ND

ELEMENT TYPE

 EV SINF      X
 EP SINF      P

ELEMENT USES

 DO I         1                        N
 XT EB(I)     SINF
 ZV EB(I)     X                        X(I)
 XP EB(I)     P         7.0
 XT EC(I)     SINF
 ZV EC(I)     X                        X(I)
 XP EC(I)     P         14.0
 ND

GROUP USES

 DO I         1                        N
 ZE FB(I)     EB(I)                    ROOT8
 ZE FB(I)     EC(I)                    ROOT6
 ND

OBJECT BOUND

*   Least square problems are bounded below by zero

 LO TRIGON2             0.0

*   Solution

*LO SOLUTION            0.0

ENDATA

***********************
* SET UP THE FUNCTION *
* AND RANGE ROUTINES  *
***********************

ELEMENTS      TRIGON2NE

TEMPORARIES

 R  D
 R  Y
 R  YX
 R  YXX
 R  S
 R  C
 M  SIN
 M  COS

INDIVIDUALS

* sin(P(X-0.9)^2

 T  SINF
 A  D                   X - 0.9
 A  Y                   D * D
 A  YX                  D + D
 A  YXX                 2.0
 A  S                   SIN( P * Y )
 A  C                   COS( P * Y )
 F                      S
 G  X                   P * C * YX
 H  X         X         - P * P * S * YX * YX + P * C * YXX

ENDATA
